import upt
import requests
from lxml import html


class MyPackage(upt.Package):
    pass


class HackageFrontend(upt.Frontend):
    name = 'hackage'

    @staticmethod
    def guess_licenses(licenses):
        hackage_to_upt = {
            'Apache-2.0': upt.licenses.ApacheLicenseTwoDotZero,
            'Artistic-2.0': upt.licenses.ArtisticLicenseTwoDotZero,
            '2-clause BSDL': upt.licenses.BSDTwoClauseLicense,
            'BSD 2-Clause': upt.licenses.BSDTwoClauseLicense,
            'BSD-2-Clause': upt.licenses.BSDTwoClauseLicense,
            'BSD-2': upt.licenses.BSDTwoClauseLicense,
            'BSD 3-Clause': upt.licenses.BSDThreeClauseLicense,
            'BSD-3-Clause': upt.licenses.BSDThreeClauseLicense,
            'BSD-3': upt.licenses.BSDThreeClauseLicense,
            'GPL-2': upt.licenses.GNUGeneralPublicLicenseTwo,
            'GPL-2.0': upt.licenses.GNUGeneralPublicLicenseTwo,
            'GPL-2.0+': upt.licenses.GNUGeneralPublicLicenseTwoPlus,
            'LGPLv2': upt.licenses.GNULesserGeneralPublicLicenseTwoDotZero,
            'LGPLv2+':
                upt.licenses.GNULesserGeneralPublicLicenseTwoDotZeroPlus,
            'LGPL-2.1':
                upt.licenses.GNULesserGeneralPublicLicenseTwoDotOne,
            'LGPLv3+': upt.licenses.GNUGeneralPublicLicenseThreePlus,
            'MIT': upt.licenses.MITLicense,
            'MPL-2.0': upt.licenses.MozillaPublicLicenseTwoDotZero,
            'Ruby': upt.licenses.RubyLicense,
        }

        return [hackage_to_upt.get(l, upt.licenses.UnknownLicense)()
                for l in licenses]

    @staticmethod
    def get_archives(release):
        url = 'https://hackage.haskell.org' + release
        archive = upt.Archive(url)
        return [archive]

    @staticmethod
    def compute_requirements(dependencies):
        requirements = {
            'run' : []
        }
        for dep in dependencies:
            name = dep.xpath('a/text()')[0]
            try:
                spec = dep.xpath('text()')[0].strip().split(
                    '(')[1].split(')')[0]
            except:
                spec = ''
            if 'details' not in name:
                pkg_req = upt.PackageRequirement(name, spec)
                requirements['run'].append(pkg_req)

        return requirements

    def parse(self, pkg_name):
        url = f'https://hackage.haskell.org/package/{pkg_name}'
        r = requests.get(url)
        if not r.ok:
            raise upt.InvalidPackageNameError(self.name, pkg_name)
        tree = html.fromstring(r.content)

        # pkg_name = tree.xpath('//div[@id = "content"]/h1/a/text()')[0]
        description = tree.xpath('//div[@id = "description"]/p/text()')[0]
        licenses = tree.xpath('//tr[th = "License"]/td/a/text()')
        version = tree.xpath('//tr[tr = starts-with(text(),"Versions")]/td/strong/text()')[0]
        homepage = tree.xpath('//tr[th = "Home page"]/td/a/@href')[0]
        download_url = tree.xpath('//div[@id = "downloads"]//li/a/@href')[0]
        dependencies = tree.xpath('//tr[th = "Dependencies"]/td/span')

        requirements = self.compute_requirements(dependencies)
        d = {
            'homepage': homepage,
            'summary': description,
            'requirements': requirements,
            'licenses': self.guess_licenses(licenses),
            'archives': self.get_archives(download_url),
        }
        return MyPackage(pkg_name, version, **d)
