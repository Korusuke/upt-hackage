import unittest

# import requests_mock

import upt
from upt_hackage.upt_hackage import HackageFrontend


class TestHackageFrontend(unittest.TestCase):
    def setUp(self):
        self.frontend = HackageFrontend()
        self.pkg_name = 'foo'
        self.url = f'https://hackage.haskell.org/package/{self.pkg_name}'


class TestLicenses(unittest.TestCase):
    def setUp(self):
        self.frontend = HackageFrontend()

    def test_no_license(self):
        out = self.frontend.guess_licenses([])
        expected = []
        self.assertListEqual(out, expected)

    def test_one_license(self):
        out = self.frontend.guess_licenses(['BSD-3-Clause'])
        expected = [upt.licenses.BSDThreeClauseLicense()]
        self.assertListEqual(out, expected)

        out = self.frontend.guess_licenses(['whatever'])
        expected = [upt.licenses.UnknownLicense()]
        self.assertListEqual(out, expected)

    def test_multiple_licenses(self):
        out = self.frontend.guess_licenses(['BSD-3-Clause', 'whatever'])
        expected = [upt.licenses.BSDThreeClauseLicense(),
                    upt.licenses.UnknownLicense()]
        self.assertListEqual(out, expected)


class TestArchives(unittest.TestCase):
    def setUp(self):
        self.frontend = HackageFrontend()

    def test_get_archives(self):
        out = self.frontend.get_archives('/source.tar.gz')
        self.assertEqual(len(out), 1)
        self.assertEqual(
            out[0].url, 'https://hackage.haskell.org/source.tar.gz')


if __name__ == '__main__':
    unittest.main()
